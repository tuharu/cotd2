<!--
Author: StefanoPicozzi@gmail.com
Blog: https://StefanoPicozzi.blog
GitHub: https://github.com/StefanoPicozzi/cotd.git
Date: 2016
-->

<?php

// Change item rankings and related attribute data here
// If adding new entries than also add associated image

$_SESSION['item'] = array();
$Rank = 1;
$ImageCnt = 0;

$_SESSION['item'][$ImageCnt]['name'] = 'adelaide';
$_SESSION['item'][$ImageCnt]['rank'] = $Rank;
$_SESSION['item'][$ImageCnt]['caption'] = 'Adelaide Cat';
$_SESSION['item'][$ImageCnt]['trivia'] = "<p>My name is Le Cornu and I live in Adelaide. My dad plays for the Adelaide Crows. He has a big mullet which I snuggle into when he is asleep.</p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can watch the footie together. </p>";
$_SESSION['item'][$ImageCnt]['theme'] = 'canaria';
$_SESSION['item'][$ImageCnt]['filename'] = 'data/images/canaria/adelaide.jpg';
$_SESSION['item'][$ImageCnt]['rating'] = 0;

$ImageCnt = $ImageCnt + 1;
$Rank = $Rank + 1;
$_SESSION['item'][$ImageCnt]['name'] = 'canberra';
$_SESSION['item'][$ImageCnt]['rank'] = $Rank;
$_SESSION['item'][$ImageCnt]['caption'] = 'canberra';
$_SESSION['item'][$ImageCnt]['trivia'] = "<p>My name is Le Cornu and I live in Adelaide. My dad plays for the Adelaide Crows. He has a big mullet which I snuggle into when he is asleep.</p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can watch the footie together. </p>";
$_SESSION['item'][$ImageCnt]['theme'] = 'canaria';
$_SESSION['item'][$ImageCnt]['filename'] = 'data/images/canaria/canberra.jpg';
$_SESSION['item'][$ImageCnt]['rating'] = 0;

$ImageCnt = $ImageCnt + 1;
$Rank = $Rank + 1;
$_SESSION['item'][$ImageCnt]['name'] = 'canberra_city';
$_SESSION['item'][$ImageCnt]['rank'] = $Rank;
$_SESSION['item'][$ImageCnt]['caption'] = 'canberra_city';
$_SESSION['item'][$ImageCnt]['trivia'] = "<p>My name is Le Cornu and I live in Adelaide. My dad plays for the Adelaide Crows. He has a big mullet which I snuggle into when he is asleep.</p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can watch the footie together. </p>";
$_SESSION['item'][$ImageCnt]['theme'] = 'canaria';
$_SESSION['item'][$ImageCnt]['filename'] = 'data/images/canaria/canberra_city.jpg';
$_SESSION['item'][$ImageCnt]['rating'] = 0;

/*
$ImageCnt = $ImageCnt + 1;
$Rank = $Rank + 1;
$_SESSION['item'][$ImageCnt]['name'] = 'roxy';
$_SESSION['item'][$ImageCnt]['rank'] = $Rank;
$_SESSION['item'][$ImageCnt]['caption'] = 'roxy';
$_SESSION['item'][$ImageCnt]['trivia'] = "<p>My name is Le Cornu and I live in Adelaide. My dad plays for the Adelaide Crows. He has a big mullet which I snuggle into when he is asleep.</p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can watch the footie together. </p>";
$_SESSION['item'][$ImageCnt]['theme'] = 'canaria';
$_SESSION['item'][$ImageCnt]['filename'] = 'data/images/canaria/roxy.jpg';
$_SESSION['item'][$ImageCnt]['rating'] = 0;
*/
?>
