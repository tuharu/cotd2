<!--
Author: StefanoPicozzi@gmail.com
Blog: https://StefanoPicozzi.blog
GitHub: https://github.com/StefanoPicozzi/cotd.git
Date: 2016
-->

<?php

// Change item rankings and related attribute data here
// If adding new entries than also add associated image

$_SESSION['item'] = array();

$_SESSION['item'][0]['name'] = 'billie';
$_SESSION['item'][0]['rank'] = 1;
$_SESSION['item'][0]['caption'] = 'billie';
$_SESSION['item'][0]['trivia'] = "<p>My name is Le Cornu and I live in Adelaide. My dad plays for the Adelaide Crows. He has a big mullet which I snuggle into when he is asleep.</p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can watch the footie together. </p>";
$_SESSION['item'][0]['theme'] = 'pets';
$_SESSION['item'][0]['filename'] = 'data/images/pets/billie.jpg';
$_SESSION['item'][0]['rating'] = 0;

$_SESSION['item'][1]['name'] = 'billy';
$_SESSION['item'][1]['rank'] = 2;
$_SESSION['item'][1]['caption'] = 'billy';
$_SESSION['item'][1]['trivia'] = "<p>My name is Burley and my post office box is in Canberra. The Government appointed me into a senior position at the Human Rights Commission. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can obsess over repealing section 18C together.</p>";
$_SESSION['item'][1]['theme'] = 'pets';
$_SESSION['item'][1]['filename'] = 'data/images/pets/billy.jpg';
$_SESSION['item'][1]['rating'] = 0;

$_SESSION['item'][2]['name'] = 'billy_2';
$_SESSION['item'][2]['rank'] = 3;
$_SESSION['item'][2]['caption'] = 'billy_2';
$_SESSION['item'][2]['trivia'] = "<p>My name is Rialto and my house is in Melbourne. I like to go to Philosophy Meetups. My favourite is Descates. He said: I think therefore I cat. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can workshop your existential mid-life crisis over some wine and cheese.</p>";
$_SESSION['item'][2]['theme'] = 'pets';
$_SESSION['item'][2]['filename'] = 'data/images/pets/billy_2.jpg';
$_SESSION['item'][2]['rating'] = 0;

$_SESSION['item'][3]['name'] = 'bogor';
$_SESSION['item'][3]['rank'] = 4;
$_SESSION['item'][3]['caption'] = 'bogor';
$_SESSION['item'][3]['trivia'] = "<p>My name is Ponsonby and I live in Auckland. I made a satellite launch vehicle using a ball of wool, 3 paper clips and a tub of bees wax. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can build a mud brick metropolis together.</p>";
$_SESSION['item'][3]['theme'] = 'pets';
$_SESSION['item'][3]['filename'] = 'data/images/pets/bogor.jpg';
$_SESSION['item'][3]['rating'] = 0;

$_SESSION['item'][4]['name'] = 'capi';
$_SESSION['item'][4]['rank'] = 5;
$_SESSION['item'][4]['caption'] = 'capi';
$_SESSION['item'][4]['trivia'] = "<p>My name is Seidler and I am from Sydney. I do not go out at night any more since they implemented the lock out laws. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can talk about Sydney property prices.</p>";
$_SESSION['item'][4]['theme'] = 'pets';
$_SESSION['item'][4]['filename'] = 'data/images/pets/capi.jpg';
$_SESSION['item'][4]['rating'] = 0;

$_SESSION['item'][5]['name'] = 'clancy';
$_SESSION['item'][5]['rank'] = 6;
$_SESSION['item'][5]['caption'] = 'clancy';
$_SESSION['item'][5]['trivia'] = "<p>My name is Gabba and I am from Brisbane. I love it here because the floods bring fish straight to my door step. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can go fishing together.</p>";
$_SESSION['item'][5]['theme'] = 'pets';
$_SESSION['item'][5]['filename'] = 'data/images/pets/clancy.jpg';
$_SESSION['item'][5]['rating'] = 0;

$_SESSION['item'][6]['name'] = 'cookie';
$_SESSION['item'][6]['rank'] = 7;
$_SESSION['item'][6]['caption'] = 'cookie';
$_SESSION['item'][6]['trivia'] =  "<p>My name is Cottlesloe and I was born in Perth. My parents work FIFO at the mines so I do not get to see them much. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and I can stay with you every second week.</p>";
$_SESSION['item'][6]['theme'] = 'pets';
$_SESSION['item'][6]['filename'] = 'data/images/pets/cookie.jpg';
$_SESSION['item'][6]['rating'] = 0;

$_SESSION['item'][7]['name'] = 'deedee';
$_SESSION['item'][7]['rank'] = 8;
$_SESSION['item'][7]['caption'] = 'deedee';
$_SESSION['item'][7]['trivia'] = "<p>My name is Mona and I am in Hobart. There is not much to do here so thank goodness for the NBN. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can watch youtube cat videos using broadband.</p>";
$_SESSION['item'][7]['theme'] = 'pets';
$_SESSION['item'][7]['filename'] = 'data/images/pets/deedee.jpg';
$_SESSION['item'][7]['rating'] = 0;

$_SESSION['item'][8]['name'] = 'elska';
$_SESSION['item'][8]['rank'] = 9;
$_SESSION['item'][8]['caption'] = 'elska';
$_SESSION['item'][8]['trivia'] = "<p>My name is Massey and I am in Wellington. I am a contender for Secretary General of the United Nations. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me unless you rather it be Kevin Rudd.</p>";
$_SESSION['item'][8]['theme'] = 'pets';
$_SESSION['item'][8]['filename'] = 'data/images/pets/elska.jpg';
$_SESSION['item'][8]['rating'] = 0;

$_SESSION['item'][9]['name'] = 'ichibaby';
$_SESSION['item'][9]['rank'] = 10;
$_SESSION['item'][9]['caption'] = 'ichibaby';
$_SESSION['item'][9]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][9]['theme'] = 'pets';
$_SESSION['item'][9]['filename'] = 'data/images/pets/ichibaby.jpg';
$_SESSION['item'][9]['rating'] = 0;

$_SESSION['item'][10]['name'] = 'max_bugger';
$_SESSION['item'][10]['rank'] = 11;
$_SESSION['item'][10]['caption'] = 'max_bugger';
$_SESSION['item'][10]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][10]['theme'] = 'pets';
$_SESSION['item'][10]['filename'] = 'data/images/pets/max_bugger.jpg';
$_SESSION['item'][10]['rating'] = 0;

$_SESSION['item'][11]['name'] = 'milky';
$_SESSION['item'][11]['rank'] = 12;
$_SESSION['item'][11]['caption'] = 'milky';
$_SESSION['item'][11]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][11]['theme'] = 'pets';
$_SESSION['item'][11]['filename'] = 'data/images/pets/milky.jpg';
$_SESSION['item'][11]['rating'] = 0;

$_SESSION['item'][12]['name'] = 'neo';
$_SESSION['item'][12]['rank'] = 13;
$_SESSION['item'][12]['caption'] = 'neo';
$_SESSION['item'][12]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][12]['theme'] = 'pets';
$_SESSION['item'][12]['filename'] = 'data/images/pets/neo.jpg';
$_SESSION['item'][12]['rating'] = 0;

$_SESSION['item'][13]['name'] = 'piggy';
$_SESSION['item'][13]['rank'] = 14;
$_SESSION['item'][13]['caption'] = 'piggy';
$_SESSION['item'][13]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][13]['theme'] = 'pets';
$_SESSION['item'][13]['filename'] = 'data/images/pets/piggy.jpg';
$_SESSION['item'][13]['rating'] = 0;

$_SESSION['item'][14]['name'] = 'roxy';
$_SESSION['item'][14]['rank'] = 15;
$_SESSION['item'][14]['caption'] = 'roxy';
$_SESSION['item'][14]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][14]['theme'] = 'pets';
$_SESSION['item'][14]['filename'] = 'data/images/pets/roxy.jpg';
$_SESSION['item'][14]['rating'] = 0;

$_SESSION['item'][15]['name'] = 'selby';
$_SESSION['item'][15]['rank'] = 16;
$_SESSION['item'][15]['caption'] = 'selby';
$_SESSION['item'][15]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][15]['theme'] = 'pets';
$_SESSION['item'][15]['filename'] = 'data/images/pets/selby.jpg';
$_SESSION['item'][15]['rating'] = 0;

$_SESSION['item'][16]['name'] = 'snuggles';
$_SESSION['item'][16]['rank'] = 17;
$_SESSION['item'][16]['caption'] = 'snuggles';
$_SESSION['item'][16]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][16]['theme'] = 'pets';
$_SESSION['item'][16]['filename'] = 'data/images/pets/snuggles.jpg';
$_SESSION['item'][16]['rating'] = 0;

$_SESSION['item'][17]['name'] = 'taala';
$_SESSION['item'][17]['rank'] = 18;
$_SESSION['item'][17]['caption'] = 'taala';
$_SESSION['item'][17]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][17]['theme'] = 'pets';
$_SESSION['item'][17]['filename'] = 'data/images/pets/taala.jpg';
$_SESSION['item'][17]['rating'] = 0;

$_SESSION['item'][18]['name'] = 'tipsy';
$_SESSION['item'][18]['rank'] = 19;
$_SESSION['item'][18]['caption'] = 'tipsy';
$_SESSION['item'][18]['trivia'] = "<p>My name is Twizel and I from Christchurch. I had a bit role in the Lord of the Rings trilogy, but so did everyone else. </p> <p style='color:silver;font-size:80%;font-style:italic'>Like me and we can geek out on LOTR trivia for hours on end. </p>";
$_SESSION['item'][18]['theme'] = 'pets';
$_SESSION['item'][18]['filename'] = 'data/images/pets/tipsy.jpg';
$_SESSION['item'][18]['rating'] = 0;